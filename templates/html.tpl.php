<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7 no-js"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8 no-js"><![endif]-->
<!--[if IE 8]><html class="lt-ie9 no-js"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php //print $head; ?>
	<title><?php print $head_title; ?></title>
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">	
	<meta http-equiv="cleartype" content="on">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php print ngwebapp_icon(); ?>
	<?php print ngwebapp_meta();?>
	<?php print ngwebapp_css();?>
	<?php print ngwebapp_scripts();?>	
</head>
<body>
<?php print $page; ?>
</body>
</html>
